//
// Created by mw on 19.03.23.
//

#include "gtest/gtest.h"

#include "../lib/GLLibrary.h"

#include <vector>
#include <list>

namespace {

    static unsigned int callback_cnt = 0;

    static void callback(const GLLib::Data& data) {
        std::cout << "Callback called with data: [";
        std::cout << data;
        std::cout << "]" << std::endl;
        callback_cnt++;
    }

    static constexpr unsigned int kNValue = 5;
    static constexpr unsigned int kThreshold = 13;

    class LibraryTests : public ::testing::Test {
    protected:

        void SetUp() override {
            callback_cnt = 0;
        }

        void TearDown() override {
        }

    };

// Tests the Increment() method.

    TEST_F(LibraryTests, VectorTests) {
        std::vector<GLLib::Data> dataVector = {
                {7, 7.7},
                {8, 8.8},
                {9, 9.9},
                {10, 10.10},
                {11, 11.11},
                {12, 12.12},
                {13, 13.13},
                {19, 19.19},
                {26, 26.26},
                {20, 20.20},
                {21, 21.21},
                {22, 22.22},
                {23, 23.23},
                {24, 24.24},
                {1, 12.1},
                {2, 23.2},
                {3, 34.3},
                {4, 4.4},
                {5, 5.5},
                {6, 6.6},
                {25, 25.25},
                {27, 27.27},
                {14, 14.14},
                {15, 15.15},
                {16, 16.16},
                {17, 17.17},
                {18, 18.18},
                {28, 28.28},
                {29, 29.29},
                {30, 30.30}
        };

        GLLib::process_container(dataVector, callback, kThreshold, kNValue);
        EXPECT_EQ(5, callback_cnt);
    }

    TEST_F(LibraryTests, ListTest) {
        std::list<GLLib::Data> dataList = {
                {7, 7.7},
                {8, 8.8},
                {9, 9.9},
                {10, 10.10},
                {11, 11.11},
                {12, 12.12},
                {13, 13.13},
                {19, 19.19},
                {26, 26.26},
                {20, 20.20},
                {21, 21.21},
                {22, 22.22},
                {23, 23.23},
                {24, 24.24},
                {1, 12.1},
                {2, 23.2},
                {3, 34.3},
                {4, 4.4},
                {5, 5.5},
                {6, 6.6},
                {25, 25.25},
                {27, 27.27},
                {14, 14.14},
                {15, 15.15},
                {16, 16.16},
                {17, 17.17},
                {18, 18.18},
                {28, 28.28},
                {29, 29.29},
                {30, 30.30}
        };

        GLLib::process_container(dataList, callback, kThreshold, kNValue);
        EXPECT_EQ(5, callback_cnt);
    }

    TEST_F(LibraryTests, DequeTest) {
        std::deque<GLLib::Data> dataDeque = {
                {7, 7.7},
                {8, 8.8},
                {9, 9.9},
                {10, 10.10},
                {11, 11.11},
                {12, 12.12},
                {13, 13.13},
                {19, 19.19},
                {26, 26.26},
                {20, 20.20},
                {21, 21.21},
                {22, 22.22},
                {23, 23.23},
                {24, 24.24},
                {1, 12.1},
                {2, 23.2},
                {3, 34.3},
                {4, 4.4},
                {5, 5.5},
                {6, 6.6},
                {25, 25.25},
                {27, 27.27},
                {14, 14.14},
                {15, 15.15},
                {16, 16.16},
                {17, 17.17},
                {18, 18.18},
                {28, 28.28},
                {29, 29.29},
                {30, 30.30}
        };

        GLLib::process_container(dataDeque, callback, kThreshold, kNValue);
        EXPECT_EQ(5, callback_cnt);
    }

    TEST_F(LibraryTests, EmptyVectorTest) {
        std::vector<GLLib::Data> dataVector = {};
        GLLib::process_container(dataVector, callback, kThreshold, kNValue);
        EXPECT_EQ(0, callback_cnt);
    }

    TEST_F(LibraryTests, EmptyListTest) {
        std::list<GLLib::Data> dataList = {};
        GLLib::process_container(dataList, callback, kThreshold, kNValue);
        EXPECT_EQ(0, callback_cnt);
    }

    TEST_F(LibraryTests, EmptyDequeTest) {
        std::deque<GLLib::Data> dataDeque = {};
        GLLib::process_container(dataDeque, callback, kThreshold, kNValue);
        EXPECT_EQ(0, callback_cnt);
    }

} // namespace
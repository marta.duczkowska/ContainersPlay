//
// Created by mw on 19.03.23.
//

#pragma once

#ifndef GL_TASK_GLLIBRARY_H
#define GL_TASK_GLLIBRARY_H

#include <algorithm>
#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <concepts>

namespace GLLib {

    enum class SortOrder {
        Ascending,
        Descending
    };

    struct Data {
        std::uint8_t x;
        double y;
    };

    // Define callback type
    typedef void (*Callback)(const Data& data);

    bool isDataXValueLess(const Data& a, const Data& b) {
        return a.x < b.x;
    }

    bool isDataYValueLess(const Data& a, const Data& b) {
        return a.y < b.y;
    }

    std::ostream& operator<<(std::ostream& os, const Data& data) {
        os << "x: " << static_cast<int>(data.x) << " y: " << data.y;
        return os;
    }

    template <typename Container>
    void sortContainerByDataXValue(Container& container, SortOrder order = SortOrder::Ascending) {
        using ContainerType = typename Container::value_type;
        using IteratorType = typename Container::iterator;

        if constexpr (std::is_same_v<Container, std::list<ContainerType>>) {
            if (order == SortOrder::Ascending) {
                container.sort(isDataXValueLess);
            } else {
                container.sort([](const Data& a, const Data& b) {
                    return !isDataXValueLess(a, b);
                });
            }
        } else {
            if (order == SortOrder::Ascending) {
                std::sort(container.begin(), container.end(), isDataXValueLess);
            } else {
                std::sort(container.begin(), container.end(), [](const Data& a, const Data& b) {
                    return !isDataXValueLess(a, b);
                });
            }
        }
    }

    template <typename Container>
    void sortContainerByDataYValue(Container& container, SortOrder order = SortOrder::Ascending) {
        using ContainerType = typename Container::value_type;
        using IteratorType = typename Container::iterator;

        if constexpr (std::is_same_v<Container, std::list<ContainerType>>) {
            if (order == SortOrder::Ascending) {
                container.sort(isDataYValueLess);
            } else {
                container.sort([](const Data& a, const Data& b) {
                    return !isDataYValueLess(a, b);
                });
            }
        } else {
            if (order == SortOrder::Ascending) {
                std::sort(container.begin(), container.end(), isDataYValueLess);
            } else {
                std::sort(container.begin(), container.end(), [](const Data& a, const Data& b) {
                    return !isDataYValueLess(a, b);
                });
            }
        }
    }

    // Make concept, which allow to use all sequential containers.
    template<typename T>
    concept SequentialContainer = requires(T container) {
        typename T::value_type;
        typename T::size_type;
        typename T::iterator;
        { container.size() } -> std::same_as<typename T::size_type>;
        { container.begin() } -> std::same_as<typename T::iterator>;
        { container.end() } -> std::same_as<typename T::iterator>;
    };

    template<SequentialContainer ContainerType>
    void showContainer(const ContainerType& container, const std::string& label) {
        std::cout << label << std::endl;
        for (const auto& item : container) {
            std::cout << " " << item << std::endl;
        }
        std::cout << std::endl;
    }

    template<SequentialContainer ContainerType>
    void callCallbacks(const ContainerType& container, Callback callback) {
        for (const auto& item : container) {
            callback(item);
        }
    }

    template<SequentialContainer ContainerType>
    void process_container(ContainerType& container, Callback callback, std::uint8_t threshold, unsigned int n) {

        sortContainerByDataXValue(container, SortOrder::Descending);


        // Find first element that is smaller or equal to threshold
        auto it = std::find_if(container.begin(), container.end(), [threshold](const Data& data) {
            return data.x <= threshold;
        });

        // Get subset A and B
        ContainerType subset_a = {container.begin(), it};
        ContainerType subset_b = {it, container.end()};

        // Show subset A:
        showContainer(subset_a, "Subset A:");

        // Show subset B:
        showContainer(subset_b, "Subset B:");

        // Process subset B (sort from smallest y to biggest y)
        sortContainerByDataYValue(subset_b);

        // Get subset of n elements from subset B
        // Get subset of n elements from subset B
        auto subset_c_begin = subset_b.begin();
        auto subset_c_end = subset_b.begin();
        std::advance(subset_c_end, std::min<size_t>(n, subset_b.size())); // Advance by n or the size of subset_b, whichever is smaller
        ContainerType subset_c = {subset_c_begin, subset_c_end};

        // Show subset C:
        showContainer(subset_c, "Subset C:");

        // Call callback for each element in subset C
        callCallbacks(subset_c, callback);
    }

} // GLLib

#endif //GL_TASK_GLLIBRARY_H
